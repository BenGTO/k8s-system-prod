#!/usr/bin/bash

for SECRET in $(find . -name "private_*"); do
    INFILE=$(basename $SECRET)
    OUTFILE=$(echo $INFILE | grep -o "[[:alpha:]]*\.ya*ml")
    BASEPATH=$(echo $SECRET | grep -o "^.*\/")

    echo Sealing ${BASEPATH}${INFILE}.
    /usr/local/bin/kubeseal --scope=namespace-wide --controller-name=sealed-secrets --secret-file=${BASEPATH}${INFILE} >${BASEPATH}${OUTFILE} 
done
